# Installation

-Clone:

```
$ git clone git@bitbucket.org:avatsaev/trails.git -b test
```


-Install submodules:

```
$ git pull origin master --recurse-submodules && git submodule update --init --recursive
```

-Update composer:

```
$ php composer.phar update
```

-Setup the database configuration in /config/db.php and config/development/db.php

-Create the db in your MySQL

-Migrate package tables:

```
$ oil refine migrate --packages
```

-Migrate tables:

```
$ oil refine migrate
```

-Import files from */fuel/app/migrations/dbdump/* to your DB.