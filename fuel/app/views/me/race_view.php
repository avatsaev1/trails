<ul class="nav nav-tabs">
		<li class='<?php echo Arr::get($subnav1, "index" ); ?>'><?php echo Html::anchor(Uri::create('me/index'),'My profil');?></li>
		<li class='<?php echo Arr::get($subnav1, "races_view" ); ?>'><?php echo Html::anchor(Uri::create('me/races_view?filter=participating'),'My Participations');?></li>
		<li class='<?php echo Arr::get($subnav, "races_manage" ); ?>'><?php echo Html::anchor(Uri::create('me/races_manage'),'Create and Manage Races');?></li>
	</ul>
	</ul>
</br>
<div class="panel panel-default">
			<div class="panel-heading">
			<table style="width:100%">
				<td><?php echo $race->title;?> - <?php echo $race->town;?></td>
				<td align="right"><?php	if($race->state != 'finished' and $race->state != 'closed' and $race->type != 'personal'){
					if ($participation) { echo Html::anchor('races/rejected/'.$race->id, '<i class="icon-wrench"></i> Reject Participation', array('class' => 'btn btn-small btn-danger'));}
			
					else
					{
						echo Html::anchor('races/participated/'.$race->id, '<i class="icon-wrench"></i> Participate', array('class' => 'btn btn-small btn-success'));			
					}
				}
		if($race->user_id == $this->current_user->id and $race->type == 'personal'){
			echo Html::anchor('me/race_edit/'.$race->id, '<i class="icon-wrench"></i> Edit Race', array('class' => 'btn btn-small btn-primary'));
					echo Html::anchor('races/closed/'.$race->id, '<i class="icon-wrench"></i> Closed Race', array('class' => 'btn btn-small btn-danger'));	
		}
		
		if($race->user_id == $this->current_user->id and $race->type == 'race' and $race->state != 'finished'){
			?>
				<div class="btn-group">
					<button type="button" class="btn btn-default btn-primary dropdown-toggle" data-toggle="dropdown"> Action <span class="caret"></span></button>
						<ul class="dropdown-menu" role="menu">
							<li><?php echo Html::anchor('me/race_edit/'.$race->id, '<i class="icon-wrench"></i> Edit Race');?></li>
							<li><?php echo Html::anchor('races/finished/'.$race->id, '<i class="icon-wrench"></i> Finished Race');?></li>
							<li><?php echo Html::anchor('races/closed/'.$race->id, '<i class="icon-wrench"></i> Delete Race', array('class' => 'btn-danger'));?></li>
							</ul>
							</div>
			<?php
		}
		if($race->state == 'finished'  and $race->user_id == $this->current_user->id)
		{
			?>
			<div class="btn-group">
					<button type="button" class="btn btn-default btn-primary dropdown-toggle" data-toggle="dropdown"> Action <span class="caret"></span></button>
						<ul class="dropdown-menu" role="menu">	
							<li><?php echo Html::anchor('races/closed/'.$race->id, '<i class="icon-wrench"></i> Delete Race', array('class' => 'btn-danger'));?></li>
						</ul>
			</div>
			<?php
		}
			?></td>
		</table>
		</div>
		
		<div class="panel-body">
			<table cellpadding="5px">
				<tr>
					<td><span class="label label-primary">Start date/time :</span></td><td><?php echo $race->datetime;?></td>
				</tr>
				<tr>
					<td><span class="label label-primary">Zipcode :</span></td><td><?php echo $race->zipcode;?></td>
				</tr>
				<tr>
					<td><span class="label label-primary">Race trajectory :</span></td><td><a target="_blank" href="<?php echo $race->track_url;?>">View on map</a></td>
				</tr>
				<tr>
					<td><?php echo $race->description;?></td>
				</tr>
			</table>
		</div>
		</div>
  </div>
</div>


<div class="panel panel-default">
  <div class="panel-heading">Stats:</div>
  <div class="panel-body">
  <?php if(isset($stats))
		{
    foreach ($stats as $stat):?> 
	<table cellpadding="5px">
		<tr>
			<td><span class="label label-primary">Current location :</span></td><td><?php echo $stat->current_location;?></td>
		</tr>
		<tr>
			<td><span class="label label-primary">Finish time :</span></td><td><?php echo $stat->finish_time;?></td>
		</tr>
	</table>
	<?php endforeach;
	}
	else
	{?>
		<p>Stats not available yet.</p>
	<?php
	} 
	?>
  </div>
</div>


<p><?php echo Html::anchor('me/races_view?filter=participating', 'Back'); ?></p>