<ul class="nav nav-tabs">
		<li class='<?php echo Arr::get($subnav1, "index" ); ?>'><?php echo Html::anchor(Uri::create('me/index'),'My profile');?></li>
		<li class='<?php echo Arr::get($subnav1, "races_view" ); ?>'><?php echo Html::anchor(Uri::create('me/races_view?filter=participating'),'My Participations');?></li>
		<li class='<?php echo Arr::get($subnav, "races_manage" ); ?>'><?php echo Html::anchor(Uri::create('me/races_manage'),'Create and Manage Races');?></li>
	</ul>
</br>

  <div class="panel-body">
    <ul class="nav nav-pills">
		<li class='<?php echo Arr::get($subnav, "Participating" ); ?>'><?php echo Html::anchor(Uri::create('me/races_view?filter=participating'),'Participating');?></li>
		<li class='<?php echo Arr::get($subnav, "Finished" ); ?>'><?php echo Html::anchor(Uri::create('me/races_view?filter=finished'),'Finished');?></li>
		<li class='<?php echo Arr::get($subnav, "Pending" ); ?>'><?php echo Html::anchor(Uri::create('me/races_view?filter=pending'),'Pending');?></li>
		<li class='<?php echo Arr::get($subnav, "Personal" ); ?>'><?php echo Html::anchor(Uri::create('me/races_view?filter=personal'),'Personal');?></li>
	</ul>

  </div>


<?php $input = Input::get('filter');

if($input == 'finished')
{
	 if ($participations):

	 foreach ($participations as $participation): ?>	
<div class="panel panel-default">
					<div class="panel-heading"><?php echo Html::anchor(Uri::create('me/race_view/'.$participation->race->id.'/'.$participation->id), $participation->race->title);?> - <?php echo $participation->race->town; ?></div>
					<div class="panel-body">
						<table style="width:100%">
							<tr>
								<td>
									<?php echo $participation->race->description; ?>
								</td>
								<td align="right">
									<?php echo $participation->race->datetime; ?>
								</td>
							</tr>

						</table>
						 
					</div>
				</div>
<?php endforeach; ?>

<?php else: ?>
<p>No Races.</p>

<?php endif; ?><p>

<?php
}

 if($input == 'personal')
{
	 if ($races): 

	 foreach ($races as $race): ?>	
<div class="panel panel-default">
					<div class="panel-heading"><?php echo Html::anchor(Uri::create('me/race_view/'.$race->id), $race->title);?> - <?php echo $race->town; ?></div>
					<div class="panel-body">
						<table style="width:100%">
							<tr>
								<td>
									<?php echo $race->description; ?>
								</td>
								<td align="right">
									<?php echo $race->datetime; ?>
								</td>
							</tr>

						</table>
						 
					</div>
				</div>
<?php endforeach; ?>

<?php else: ?>
<p>No Races.</p>

<?php endif; ?><p>


<?php

}


if($input == 'participating' or $input == 'pending')
{

 if ($participations):

 	foreach ($participations as $participation): ?>	
<div class="panel panel-default">
					<div class="panel-heading"><?php echo Html::anchor(Uri::create('me/race_view/'.$participation->race->id), $participation->race->title);?> - <?php echo $participation->race->town; ?> 
					</div>
					<div class="panel-body">
						<table style="width:100%">
							<tr>
								<td>
									<?php echo $participation->race->description; ?>
								</td>
								
								<td align="right">
									<?php echo $participation->race->datetime; ?>
								</td>
							</tr>

						</table>
						 
					</div>
				</div>
<?php endforeach; ?>


<?php else: ?>
<p>No Races.</p>

<?php endif; }?><p>

</p>
