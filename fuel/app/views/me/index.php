<ul class="nav nav-tabs">
		<li class='<?php echo Arr::get($subnav, "index" ); ?>'><?php echo Html::anchor(Uri::create('me/index'),'My profile');?></li>
		<li class='<?php echo Arr::get($subnav, "races_view" ); ?>'><?php echo Html::anchor(Uri::create('me/races_view?filter=participating'),'My Participations');?></li>
		<li class='<?php echo Arr::get($subnav, "races_view" ); ?>'><?php echo Html::anchor(Uri::create('me/races_manage'),'Create and Manage Races');?></li>
</ul>

<div class="row">
	<p align="right"; class='<?php echo Arr::get($subnav, "edit" ); ?>'><?php echo Html::anchor(Uri::create('me/edit'),'Edit');?></p>
</div>

<div class="row">
	<div class="panel panel-default">
            <div class="panel-body">
            
            	<div class="col-md-3">
					<div class="panel panel-default">
						<div class="panel-body"> 
							<? echo Asset::img('default_avatar.png', array('style' => "max-width:100%")); ?>   
						</div>
					</div>
				</div>
				
				<div class="col-md-4">
					<div class="panel panel-default">
						<div class="panel-heading">Informations:</div>
						<div class="panel-body">
							<table style="width:100%">
								<tr>
									<td><span class="label label-primary">First Name :</span></td>
									<td style="text-align:right"><? echo $current_user->first_name; ?></td>
								</tr>
								<tr>
									<td><span class="label label-primary">Last name :</span></td>
									<td style="text-align:right"><? echo $current_user->last_name; ?></td>
								</tr>
								<tr>
									<td><span class="label label-primary">@username :</span></td>
									<td style="text-align:right"><? echo $current_user->username; ?></td>
								</tr>
								<tr>
									<td><span class="label label-primary">Mail :</span></td>
									<td style="text-align:right"><? echo $current_user->email; ?></td>
								</tr>
							</table>
						</div>
						</div>	
					</div>
				
				
				<div class="col-md-5">
					<div class="panel panel-default">
						<div class="panel-heading">About me:</div>
						<div class="panel-body">	
							<? echo $current_user->about; ?>
						</div>
						</div>
				</div>	
			</div>
		</div>
	</div>
</div>
