<ul class="nav nav-tabs">
		<li class='<?php echo Arr::get($subnav1, "index" ); ?>'><?php echo Html::anchor(Uri::create('me/index'),'My profile');?></li>
		<li class='<?php echo Arr::get($subnav1, "races_view" ); ?>'><?php echo Html::anchor(Uri::create('me/races_view?filter=participating'),'My Participations');?></li>
		<li class='<?php echo Arr::get($subnav1, "races_manage" ); ?>'><?php echo Html::anchor(Uri::create('me/races_manage'),'Create and Manage Races');?></li>
	</ul>
</br>

<div class="panel-body">
    <ul class="nav nav-pills">
<?php
	echo Html::anchor(Uri::create('me/race_new'), 'Create new race', array('class' => 'btn btn-small btn-success'));	
?>
</div>
	<?php if ($races): ?>

<?php foreach ($races as $race): ?>	
<div class="panel panel-default">
					<div class="panel-heading">
					<table style="width:100%">
						<td><?php echo Html::anchor(Uri::create('me/race_view/'.$race->id), $race->title);?> - <?php echo $race->town; ?></td><td align="right">
						<div class="btn-group">
							<button type="button" class="btn btn-default btn-primary dropdown-toggle" data-toggle="dropdown"> Action <span class="caret"></span></button>
							<ul class="dropdown-menu" role="menu">
								<li><?php echo Html::anchor('me/race_edit/'.$race->id, '<i class="icon-wrench"></i> Edit Race');?></li>
								<li><?php echo Html::anchor('races/finished/'.$race->id, '<i class="icon-wrench"></i> Finished Race');?></li>
								<li><?php echo Html::anchor('races/closed/'.$race->id, '<i class="icon-wrench"></i> Delete Race', array('class' => 'btn-danger'));?></li>
							</ul>
						</div>
						</td>
					</table>
					</div>
					
					<div class="panel-body">
						<table style="width:100%">
							<tr>
								<td>
									<?php echo $race->description; ?>
								</td>
								<td align="right">
									<?php echo $race->datetime; ?>
								</td>
							</tr>

						</table> 
					</div>
				</div>
<?php endforeach; ?>

<?php else: ?>
<p>No Races.</p>

<?php endif; ?><p>

</p>
