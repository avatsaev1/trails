<?php echo Form::open(array("class"=>"form-horizontal")); ?>

	<fieldset>
		<div class="form-group">
			<?php echo Form::label('Title', 'title', array('class'=>'control-label')); ?>

			<?php echo Form::input('title', Input::post('title', isset($race) ? $race->title : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Title')); ?>

		</div>

		<div class="form-group">
			<?php echo Form::label('Description', 'description', array('class'=>'control-label')); ?>

			<?php echo Form::textarea('description', Input::post('description', isset($race) ? $race->description : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Description')); ?>
		</div>


		<div class="form-group">
			<?php echo Form::label('Start Date/Time', 'datetime', array('class'=>'control-label')); ?>


			<div class='input-group date' id='datetimepicker1'>
					<?php echo Form::input('datetime', Input::post('datetime', isset($race) ? $race->datetime : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Start Date/time', 'data-format'=>"YYYY-MM-DD hh:mm:ss")); ?>
                    
                    <span class="input-group-addon">
                    	<span class="glyphicon glyphicon-calendar"></span>
                    </span>
            </div>

		</div>


		<div class="form-group">
			<?php echo Form::label('Zip code', 'zipcode', array('class'=>'control-label')); ?>

			<?php echo Form::input('zipcode', Input::post('zipcode', isset($race) ? $race->zipcode : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Zip Code...')); ?>

		</div>

		<div class="form-group">
			<?php echo Form::label('Town', 'town', array('class'=>'control-label')); ?>

			<?php echo Form::input('town', Input::post('town', isset($race) ? $race->town : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Town')); ?>

		</div>

		<?//User is a coach

			if($current_user->group_id == '8'){

				?><div class="form-group" id="race_type_view"><?
					echo Form::label('Race type:&nbsp;', 'type', array('class'=>'control-label')); 
					echo Form::select('type', Input::post('type', isset($race) ? $race->type : 'race') , array(
					    'race' => 'Race',
					    'personal' => 'Personal',
					),
					array('class' => "selectpicker" ));

				?></div><?

				?><div class="form-group"><?
					echo Form::label('Public race:&nbsp;', 'public', array('class'=>'control-label')); 
					echo Form::checkbox('public', '1', Input::post('public', isset($race) ? $race->public : '1'));
				?></div><?

				



			}


		?>

		<div class="form-group">
			<?php echo Form::label('<a target="_blank" href="http://www.gmap-pedometer.com">GPX DATA (?)</a>', 'gpx', array('class'=>'control-label')); ?>

			<?php null//echo Form::input('gpx', null, array('class' => 'col-md-4 form-control', 'placeholder'=>'GPX file', 'type' => 'file')); ?>


			<?php echo Form::textarea('gpx', Input::post('gpx', isset($race) ? $race->gpx : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Contents from .gpx file...')); ?>

		</div>

		<div class="form-group">
			<?php echo Form::label('<a target="_blank" href="http://www.gmap-pedometer.com">Track URL (?)</a>', 'track_url', array('class'=>'control-label')); ?>

			<?php echo Form::input('track_url', Input::post('track_url', isset($race) ? $race->track_url : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Track URL...')); ?>

		</div>




		<div class="form-group">
			<label class='control-label'>&nbsp;</label>
			<?php echo Form::submit('submit', 'Save', array('class' => 'btn btn-primary')); ?>		
		</div>






	</fieldset>


<?php echo Form::close(); ?>


<script type="text/javascript">

$(document).ready(function() {
    $('#datetimepicker1').datetimepicker({
    	 useSeconds: true, 
    });
    $('.selectpicker').selectpicker();
});

</script>