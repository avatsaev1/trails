<h2>Listing Countries</h2>
<br>
<?php if ($countries): ?>
<table class="table table-striped">
	<thead>
		<tr>
			<th>Title</th>
			<th>Code</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($countries as $item): ?>		<tr>

			<td><?php echo $item->title; ?></td>
			<td><?php echo $item->code; ?></td>
			<td>
				<?php echo Html::anchor('admin/country/view/'.$item->id, 'View'); ?> |
				<?php echo Html::anchor('admin/country/edit/'.$item->id, 'Edit'); ?> |
				<?php echo Html::anchor('admin/country/delete/'.$item->id, 'Delete', array('onclick' => "return confirm('Are you sure?')")); ?>

			</td>
		</tr>
<?php endforeach; ?>	</tbody>
</table>

<?php else: ?>
<p>No Countries.</p>

<?php endif; ?><p>
	<?php echo Html::anchor('admin/country/create', 'Add new Country', array('class' => 'btn btn-success')); ?>

</p>
