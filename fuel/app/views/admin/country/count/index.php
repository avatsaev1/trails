<h2>Listing Country_counts</h2>
<br>
<?php if ($country_counts): ?>
<table class="table table-striped">
	<thead>
		<tr>
			<th>Title</th>
			<th>Code</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($country_counts as $item): ?>		<tr>

			<td><?php echo $item->title; ?></td>
			<td><?php echo $item->code; ?></td>
			<td>
				<?php echo Html::anchor('admin/country/count/view/'.$item->id, 'View'); ?> |
				<?php echo Html::anchor('admin/country/count/edit/'.$item->id, 'Edit'); ?> |
				<?php echo Html::anchor('admin/country/count/delete/'.$item->id, 'Delete', array('onclick' => "return confirm('Are you sure?')")); ?>

			</td>
		</tr>
<?php endforeach; ?>	</tbody>
</table>

<?php else: ?>
<p>No Country_counts.</p>

<?php endif; ?><p>
	<?php echo Html::anchor('admin/country/count/create', 'Add new Country count', array('class' => 'btn btn-success')); ?>

</p>
