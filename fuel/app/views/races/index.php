<h2>Listing Races</h2>
<br>
<?php if ($races): 

	foreach ($races as $item):
	?>	 
	 	<div class="panel panel-default">
			<div class="panel-heading">
				<table style="width:100%">
					<tr>
						<td><?php  echo Html::anchor(Uri::create('races/view/'.$item->id), $item->title); ?> - <?php echo $item->town; ?></td>
					</tr>
			</table>
			</div>
			<div class="panel-body">
				<table style="width:100%">
					<tr>		
						<td><?php echo $item->description; ?></td>
						<td align="right"><?php echo $item->datetime; ?></td>
					</tr>

				</table>
			</div>
		</div>
	<?php endforeach; ?>	

<?php else: ?>

<p>No Races.</p>

<?php endif; ?>
