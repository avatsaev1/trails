<div class="panel panel-default">
	<div class="panel-heading">
  		<table style="width:100%">
			<tr>
	<?php foreach ($races as $race):?>
				<td><?php echo $race->title;?> - <?php echo $race->town;?></td>
				<td align="right">
      <?php if ($participation) { 
					echo Html::anchor('races/rejected/'.$race->id, '<i class="icon-wrench"></i> Reject Participation', array('class' => 'btn btn-small btn-danger'));}
			
		    else {
						echo Html::anchor('races/participated/'.$race->id, '<i class="icon-wrench"></i> Participate', array('class' => 'btn btn-small btn-success'));					 }?>

				</td>
			</tr>
		</table>
	</div>
	<div class="panel-body">
		<table cellpadding="5px">
			<tr>
				<td><span class="label label-primary">Start date/time :</span></td><td><?php echo $race->datetime;?></td>
			</tr>
			<tr>
				<td><span class="label label-primary">Zipcode :</span></td><td><?php echo $race->zipcode;?></td>
			</tr>
			<tr>
				<td><span class="label label-primary">Race trajectory :</span></td><td><a target="_blank" href="<?php echo $race->track_url;?>">View on map</a></td>
			</tr>
			<tr>
				<td><?php echo $race->description;?></td>
			</tr>
		</table>
  	<?php endforeach; ?>
  </div>
</div>

<p><?php echo Html::anchor('races/index', 'Back'); ?></p>