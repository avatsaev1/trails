<div class="jumbotron">
	<p align="center"><? echo Asset::img('logo.png', array('style' => "max-width:18%")); ?></p>
	<h1 align="center">Trails</h1>
   <p align="center">This is not a simple application, it's your partner in your most beautiful sporting achievements.</p>
</div>

<div class="row">
        <div class="col-md-4 text-center" >
          <h2>Mobile application</h2>
          <p><? echo Asset::img('mobile.png', array('style' => "max-width:100%")); ?></p>
          <p>Trails can not only organized your races, but, you can also, thanks to the IOS application, check your position on the map and the position of other runners. </p>
        </div>
        <div class="col-md-4 text-center">
          <h2>Create the race that suits you best</h2>
           <p><? echo Asset::img('coord.png', array('style' => "max-width:100%")); ?></p>
          <p>With Trails, you can create and manage your race and share it with others, check your participations at public races, reject or accept participations and much more.</p>
       </div>
        <div class="col-md-4 text-center">
          <h2>Race's Stats</h2>
          <p><? echo Asset::img('stats.png', array('style' => "max-width:100%")); ?></p>
          <p>When a race is finished, you can see your performance and differents informations about the race's results.</p>
        </div>
      </div>

  </div>
</div>
