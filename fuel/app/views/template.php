<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title><?php echo $title; ?></title>
	<?php echo Asset::css(array(
							'bootstrap.css',
							'bootstrap-datetimepicker.min.css',
							'bootstrap-select.css'

							)); ?>
	<style>
		body { margin: 50px; }
	</style>
	<?php echo Asset::js(array(
		'http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js',
		'bootstrap.js',
		'moment.min.js',
		'bootstrap-datetimepicker.min.js',
		'bootstrap-select.js',
	)); ?>
	<script>
		$(function(){ $('.topbar').dropdown(); });
	</script>
</head>
<body>

	<?php if ($current_user): ?>
	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?php echo Uri::create('home/index');?>">Trails</a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					
					<?php
						
						if($current_user->group_id==6){
						
						?>
						  <li class="<?php echo Uri::segment(2) == '' ? 'active' : '' ?>">
                <?php echo Html::anchor('admin', 'Dashboard') ?>
              </li>
						<?
						
						  $files = new GlobIterator(APPPATH.'classes/controller/admin/*.php');
  						foreach($files as $file){
  							$section_segment = $file->getBasename('.php');
  							$section_title = Inflector::humanize($section_segment);
  							?>
  							<li class="<?php echo Uri::segment(2) == $section_segment ? 'active' : '' ?>">
  								<?php echo Html::anchor('admin/'.$section_segment, $section_title) ?>
  							</li>
  							<?php
              }
              
						}else{
						 ?>
						 
						  <li>
  								<a href='<?php echo Uri::create('me/index');?>'>Me</a>
  						</li>
  						
  						<li>
  								<a href=''>Notifications</a>
  						</li>
  						
  						<li>
  								<a href='<?php echo Uri::create('races/index');?>'>Races</a>
  						</li>
  						
  						<li>
  								<a href=''>Contacts</a>
  						</li>
  						
  							
  				   <?
						}
						
					?>
				</ul>
				<ul class="nav navbar-nav pull-right">
					<li class="dropdown">
						<a data-toggle="dropdown" class="dropdown-toggle" href="#"><?php echo $current_user->username ?> <b class="caret"></b></a>
						<ul class="dropdown-menu">
						  <? if($current_user->group_id==6){?>
							  <li><?php echo Html::anchor('admin/logout', 'Logout') ?></li>
							<?}else{?>
  							<li><?php echo Html::anchor('users/logout', 'Logout') ?></li>
							<?}?>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<?php 
		
	else:
	{?>
	
		<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?php echo Uri::create('home/index');?>">Trails</a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
				</ul>
				<ul class="nav navbar-nav pull-right">
					<li>
  						<a href='<?php echo Uri::create('users/login');?>'>Login</a>
  					</li>
  					<li>
  						<a href='<?php echo Uri::create('users/register');?>'>Register</a>
  					</li>
				</ul>
			</div>
		</div>
</div>
	<?php }
	endif; 
	?>

	<div class="container">
		<div class="row">
			<div class="col-md-12">
			<hr>
<?php if (Session::get_flash('success')): ?>
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<p>
					<?php echo implode('</p><p>', (array) Session::get_flash('success')); ?>
					</p>
				</div>
<?php endif; ?>
<?php if (Session::get_flash('error')): ?>
				<div class="alert alert-error alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<p>
					<?php echo implode('</p><p>', (array) Session::get_flash('error')); ?>
					</p>
				</div>
<?php endif; ?>
			</div>
			<div class="col-md-12">
<?php echo $content; ?>
			</div>
		</div>
		<hr/>
		<footer>
			<p>&copy; IUT Saint-Dié 2014</p>
		</footer>
	</div>
</body>
</html>
