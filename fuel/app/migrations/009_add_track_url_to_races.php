<?php

namespace Fuel\Migrations;

class Add_track_url_to_races
{
	public function up()
	{
		\DBUtil::add_fields('races', array(
			'track_url' => array('constraint' => 255, 'type' => 'varchar'),

		));
	}

	public function down()
	{
		\DBUtil::drop_fields('races', array(
			'track_url'

		));
	}
}