<?php

namespace Fuel\Migrations;

class Create_apiauthtokens
{
	public function up()
	{
		\DBUtil::create_table('apiauthtokens', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'token' => array('constraint' => 255, 'type' => 'varchar'),
			'user_id' => array('constraint' => 11, 'type' => 'int'),
			'apiapp_id' => array('constraint' => 11, 'type' => 'int'),
			'exp' => array('type' => 'date'),
			'state' => array('constraint' => 32, 'type' => 'varchar'),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('apiauthtokens');
	}
}