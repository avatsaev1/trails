<?php

namespace Fuel\Migrations;

class Create_races
{
	public function up()
	{
		\DBUtil::create_table('races', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'user_id' => array('constraint' => 11, 'type' => 'int'),
			'title' => array('constraint' => 50, 'type' => 'varchar'),
			'description' => array('type' => 'text'),
			'datetime' => array('type' => 'datetime'),
			'zipcode' => array('constraint' => 32, 'type' => 'varchar'),
			'town' => array('constraint' => 200, 'type' => 'varchar'),
			'coords' => array('type' => 'text'),
			'public' => array('type' => 'boolean'),
			'type' => array('constraint' => 32, 'type' => 'varchar'),
			'state' => array('constraint' => 32, 'type' => 'varchar'),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('races');
	}
}