<?php

namespace Fuel\Migrations;

class Add_gpx_to_races
{
	public function up()
	{
		\DBUtil::add_fields('races', array(
			'gpx' => array('type' => 'text'),

		));
	}

	public function down()
	{
		\DBUtil::drop_fields('races', array(
			'gpx'

		));
	}
}