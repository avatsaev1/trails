<?php

namespace Fuel\Migrations;

class Create_stats
{
	public function up()
	{
		\DBUtil::create_table('stats', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'participation_id' => array('constraint' => 11, 'type' => 'int'),
			'race_id' => array('constraint' => 11, 'type' => 'int'),
			'current_location' => array('constraint' => 64, 'type' => 'varchar'),
			'path' => array('type' => 'text'),
			'finish_time' => array('type' => 'datetime'),
			'state' => array('constraint' => 64, 'type' => 'varchar'),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('stats');
	}
}