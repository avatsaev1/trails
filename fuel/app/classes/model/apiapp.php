<?php

class Model_Apiapp extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'api_key',
		'name',
		'state',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);
	
	protected static $_table_name = 'apiapps';

	protected static $_has_many = array(
		'apiauthtokens',
	);





}
