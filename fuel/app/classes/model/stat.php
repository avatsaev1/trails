<?php

class Model_Stat extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'participation_id',
		'race_id',
		'current_location',
		'path',
		'finish_time',
		'state',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);
	protected static $_table_name = 'stats';

	protected static $_belongs_to = array('race','participation');

}
