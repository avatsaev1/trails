<?php

class Model_Race extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'user_id',
		'title',
		'description',
		'datetime',
		'zipcode',
		'town',
		'coords',
		'public',
		'type',
		'state',
		'created_at',
		'updated_at',
		'gpx',
		'track_url',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	public static function validate($factory){

		$val = Validation::forge($factory);
		$val->add_field('title', 'Title', 'required|max_length[32]');
		$val->add_field('description', 'Description', 'required|max_length[500]');

		return $val;
	}
	
	
	protected static $_has_many = array(
										'participations'=>array(
											'cascade_delete' => true,
										),
										'stats',
									);


	protected static $_belongs_to = array('user');

}
