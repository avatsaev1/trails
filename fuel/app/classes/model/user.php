<?php

class Model_User extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'first_name',
		'last_name',
		'username',
		'password',
		'email',
		'group_id',
        'about',
		'last_login',
		'login_hash',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	
	protected static $_has_many = array(
        'participations', 
        'races',
    );
	
	//Login
	public static function login(Fieldset $form){
	
        $form->add('username', 'Username:')->add_rule('required');
        $form->add('password', 'Choose Password:', array('type'=>'password'))->add_rule('required');
        $form->add('submit', ' ', array('type'=>'submit', 'value' => 'Register'));
    
        return $form;
    }
	
	public static function register(Fieldset $form){
	
        $form->add('username', 'Username:')->add_rule('required');
        $form->add('first_name', 'First name:')->add_rule('required');
        $form->add('last_name', 'Last name:')->add_rule('required');
        $form->add('password', 'Choose Password:', array('type'=>'password'))->add_rule('required');
        $form->add('password2', 'Re-type Password:', array('type' => 'password'))->add_rule('required');
        $form->add('email', 'E-mail:')->add_rule('required')->add_rule('valid_email');
    
        $form->add('group_id', 'User type:', array('class'=> 'selectpicker','type'=>'select', 'options'=>array(7=>'Runner', 8=>'Trainer')))->add_rule('required');
        $form->add('about', 'About:', array('type' => "textarea" ))->add_rule('required');
        $form->add('submit', ' ', array('type'=>'submit', 'value' => 'Register'));
        return $form;
    }
  
  
  public static function validate_registration(Fieldset $form, $auth){
  
    $form->field('password')->add_rule('match_value', $form->field('password2')->get_attribute('value'));
    $form->field('group_id')->add_rule('numeric_between', 7, 8);
    
    $val = $form->validation();
    $val->set_message('required', 'The field :field is required.');
    $val->set_message('valid_email', 'The field :field must be an e-mail address.');
    $val->set_message('match_value', 'The passwords must match.');
    $val->set_message('match_value', 'Invalid user type.');
 
    if ($val->run()){

        $first_name = $form->field('first_name')->get_attribute('value');
        $last_name = $form->field('last_name')->get_attribute('value');
        $username = $form->field('username')->get_attribute('value');
        $password = $form->field('password')->get_attribute('value');
        $email = $form->field('email')->get_attribute('value');
        $group_id = $form->field('group_id')->get_attribute('value');
        $about = $form->field('about')->get_attribute('value');

        try
        {
            $user = $auth->create_user($username, $password, $email, $group_id, array('first_name'=>$first_name, 'last_name'=>$last_name, 'about' => $about ));
        }
        catch (Exception $e)
        {
            $error = $e->getMessage();
        }
 
        if (isset($user))
        {
            $auth->login($username, $password);
        }
        else
        {
            if (isset($error))
            {
                $li = $error;
            }
            else
            {
                $li = 'Something went wrong with creating the user!';
            }
            $errors = Html::ul(array($li));
            return array('e_found' => true, 'errors' => $errors);
        }
    }
    else
    {
        $errors = $val->show_errors();
        return array('e_found' => true, 'errors' => $errors);
    }
  
  }
  

}
