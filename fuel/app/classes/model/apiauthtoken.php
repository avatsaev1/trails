<?php

class Model_Apiauthtoken extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'token',
		'user_id',
		'apiapp_id',
		'exp',
		'state',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);
	protected static $_table_name = 'apiauthtokens';


	protected static $_belongs_to = array(
		'apiapp',
		'user',
	);




}
