<?php

/**********************************************
	Trails REST API
	Author: Vatsaev Aslan (@avatsaev)
	For project: Trails, IUT de Saint-Dié
	Date: March-2014
	Version: 0.1 
***********************************************/

//---------------------------------------------------------------------------

//Default API KEY: 8f8f647a78104a5085967562d98f0d88

//---------------------------------------------------------------------------

//ERROR CODES
	//EINVAL: 	Required params missing
	//EWAPI: 	Wrong api key
	//EWAUTH:	Wrong auth token
	//EWRENT:	Error while writing/deleting entry
	//EWUSRPWD:	Wrong username/password
	//ENOENT:	No such entry
	//EPERM:	Permission error. Operation not permitted.
	//EACCES:	Access error. Entry not available.

//----------------------------------------------------------------------------

//ENDPOINTS
/*
Every request requires api_key param (ex: /endpoint?api_key=MY_API_KEY)
Some requests require user authetication token (ex: /endpoint?api_key=MY_API_KEY&token=USER_AUTH_TOKEN)
	
	Get public races: 						/races

	Get public race info:					/race/:id

	Login user:								/login
											params: username , password

	Get user's participations:				/user_participations
											params: token

	Accept participation to a public race:	/race/:id/accept_participation
											params: token

	Reject participation to a public race:	/race/:id/reject_participation
											params: token

	Get user's participation at race:		/race/:id/user_participation
											params: token
*/

//----------------------------------------------------------------------------

class Controller_Api1 extends Controller_Rest
{

	protected $format = 'json';
	protected $apiapp = null;
	protected $apiauthtoken = null;


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	private function verify_api_app(){

		if( Input::get('api_key') == null ){

			//wrong/inexistent required parameters

			return $this->response(
				array(
    				'status' => array(	
    					'type'=>'error', 
    					'msg'=>'Inexistent required parameters: (api_key)',
    					'ERRNO'=>'EINVAL'
    				),
    			),
    			400
    		);

    		$this->apiapp=null;

    		exit();
		}


		 $apiapp_  = DB::select()->from('apiapps')->as_object('Model_Apiapp')->where_open()
							->where('api_key', Input::get('api_key'))
						->where_close()->execute()->as_array();

		if(!isset($apiapp_[0]) || empty($apiapp_[0]) ){

			//wrong api key
			return $this->response(
				array(
        			'status' => array(	
        				'type'=>'error', 
        				'msg'=>'Wrong API key.',
        				'ERRNO'=>'EWAPI'
        			),
        		),
        		400
        	);

        	$this->apiapp=null;

        	exit();
		}

		$this->apiapp = $apiapp_[0];
	}


	private function verify_api_authtoken(){


		if( Input::get('token') == null ){

			//wrong/inexistent required parameters

			return $this->response(
				array(
    				'status' => array(
    					'type'=>'error',
    					'msg'=>'Inexistent required parameters: (token)',
    					'ERRNO'=>'EINVAL'
    				),
    			),
    			400
    		);

    		$this->apiauthtoken = null;

    		exit();
		}


		$apiauthtoken_  = DB::select()->from('apiauthtokens')->as_object('Model_Apiauthtoken')->where_open()
							->where('token', Input::get('token'))
							->and_where('apiapp_id', $this->apiapp->id)
						->where_close()->execute()->as_array();

		if(!$apiauthtoken_){

			return $this->response(
				array(
    				'status' => array(
    					'type'=>'error', 
    					'msg'=>'Wrong auth token.',
    					'ERRNO'=>'EWAUTH'
    				),
    			),
    			400
    		);

			$this->apiauthtoken = null;

    		exit();

		}

		$this->apiauthtoken=$apiauthtoken_[0] ;

	}


	public function before(){
		parent::before();
		$this->verify_api_app();
	}



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	public function get_login(){


		if(!$this->apiapp){
			return $this->response;
			exit();
		}

		if(Input::get('username') == null || Input::get('password') == null){
			//wrong/inexistent required parameters

			return $this->response(
				array(
    				'status' => array(
    					'type'=>'error', 
    					'msg'=>'Inexistent required parameters: (username, password).',
    					'ERRNO'=>'EINVAL'
    				),
    			),
    			400
    		);
    		exit();
		}

		$auth = Auth::instance();

		//check username and password
		if($auth->login(Input::get('username'), Input::get('password'))){

			
			$current_user = Auth::check() ? Model\Auth_User::find_by_username(Auth::get_screen_name()) : null;
			
			if(isset($current_user) && !empty($current_user)){



				//check if this app already indetified this user
				//@TODO (done?): can't get the result as object (instead of array) when using WHERE clause, need to get it as object
				$apiauthtoken_  = DB::select()->from('apiauthtokens')->as_object('Model_Apiauthtoken')->where_open()
									->where('user_id', $current_user->id )
									->and_where('apiapp_id', $this->apiapp->id)
								->where_close()->execute()->as_array();


				//if this app never identified this user before, create new user token for this app
				if(!$apiauthtoken_){

					$apiauthtoken_=Model_Apiauthtoken::forge(array(
						'token' => uniqid(),
						'user_id' => $current_user->id,
						'apiapp_id' => $this->apiapp->id,
						'exp' => strtotime('+3 years'),
						'state' => 'enabled',
					));

					if(!$apiauthtoken_->save()){ //if couldn't create new token in database 

						return $this->response(
							array(
			    				'status' => array(
			    					'type'=>'error',
			    					'msg'=>'Couldn\'t create new token for this user.',
			    					'ERRNO' => 'EWRENT',
			    				),
			    			),
			    			500
			    		);

			    		exit();
					}

					$this->apiauthtoken = $apiauthtoken_;

				}

				$this->apiauthtoken = $apiauthtoken_[0];

				//everything went ok
				return $this->response(
					array(
            			'status' => array(
            				'type'=>'ok',
            				'msg'=>'Logged in successfully.',
            				'ERRNO'=> null,
            			),
            			'user' => array(
            				'authtoken' => $this->apiauthtoken->token,
            				'username'=>$current_user->username,
            				'first_name'=>$current_user->first_name,
            				'last_name'=>$current_user->last_name,
            				'about'=>$current_user->about,
            				'email'=>$current_user->email,
            				'group'=>$current_user->group_id,
            			),
            		),
            		200
        		);

			}else{
				//user error
			}

		}else{
			//auth error

			return $this->response(
				array(
    				'status' => array(
    					'type'=>'error',
    					'msg'=>'Wrong username or password',
    					'ERRNO' => 'EWUSRPWD',
    				),
    			),
    			400
    		);
    		exit();

		}
	}


	public function get_user_participations(){

		if(!$this->apiapp){
			return $this->response;
			exit();
		}

		$this->verify_api_authtoken();


		if(!$this->apiauthtoken){
			return $this->response;
			exit();
		}

		$participation_array = array();

		foreach ( $this->apiauthtoken->user->participations as $participation) {

			$tag = null;

			if($participation->race->type == "personal"){
				$tag='personal';

			}else if ($participation->race->type == "race"){
				
				$tag='race';

				if($participation->race->user_id == $this->apiauthtoken->user->id){
					$tag = "race_owner";
				}

			}

			$race_ = Model_Race::find($participation->race->id, array('related' => array('participations','user')));

			array_push($participation_array, array(
												'state'=>$participation->state,
												'race' =>$race_,
												'tag' => $tag,
											)
			);
			
		}

		//everything went ok
		return $this->response(
			array(
    			'status' => array(
    				'type'=>'ok',
    				'msg'=>'Listing participations.',
    				'ERRNO'=>null,
    			),
    			'participations' => $participation_array,
    		),
    		200
		);

	}

	public function get_user_accept_participation($race_id=null){

		if(!$this->apiapp){
			return $this->response;
			exit();
		}

		$this->verify_api_authtoken();

		if(!$this->apiauthtoken){
			return $this->response;
			exit();
		}

		if($race_id == null){
			//wrong/inexistent required parameters

			return $this->response(
				array(
    				'status' => array(
    					'type'=>'error',
    					'msg'=>'Inexistent required parameters: (race_id).',
    					'ERRNO'=> 'EINVAL',
    				),
    			),
    			400
    		);
    		exit();
		}

		$race = Model_Race::find($race_id);

		if(!$race){

			return $this->response(
				array(
    				'status' => array(
    					'type'=>'error',
    					'msg'=>'Race not found ('.$race_id.').',
    					'ERRNO' => 'ENOENT',
    				),
    			),
    			404
    		);
    		exit();
		}

		if(!$race->public || $race->type != "race"){
			return $this->response(
				array(
					'status' => array(
						'type'=>'error',
						'msg'=>'This race is not public.',
						'ERRNO' => 'EPERM',
					),
				),
				400
			);
			exit();
		}


		if( $race->state != "enabled"){

			return $this->response(
				array(
					'status' => array(
						'type'=>'error',
						'msg'=>'This race is not available: '.$race->state,
						'ERRNO'=>'EACCES',
					),
				),
				400
			);
			exit();
		}

		$participation = DB::select()->from('participations')->as_object('Model_Participation')->where_open()
			->where('user_id', $this->apiauthtoken->user->id)
			->and_where('race_id', $race->id)
		->where_close()->execute()->as_array();


		if($participation && $participation[0]->state=='participating'){

			return $this->response(
				array(
            		'status' => array(
            			'type'=>'ok',
            			'msg'=>'User is already participating.',
            			'ERRNO'=>null,
            		),
            		'participation'=>$participation[0]
            	),
            	200
       		);

		}else if(!$participation){ //create a new participation

			$participation=Model_Participation::forge(array(
						'user_id' => $this->apiauthtoken->user->id,
						'race_id' => $race->id,
						'state' => 'participating',
			));

			if(!$participation->save()){ 
				//error while creating a new participation
				return $this->response(
					array(
						'status' => array(
							'type'=>'error',
							'msg'=>'Error while creating participation.',
							'ERRNO'=>'EWRENT',
						),
					),
					500
				);
				exit();				


			}else{
				//everything went well

				
				$stat = Model_Stat::forge(array(
						'participation_id' => $participation->id,
						'race_id' => $race->id,
						'current_location' => '',
						'path' => '',
						'finish_time' => '',
						'state' => 'enabled',

				));

				if(!$stat->save()){

					return $this->response(
						array(
							'status' => array(
								'type'=>'error',
								'msg'=>'Error while creating participation stat.',
								'ERRNO'=>'EWRENT',
							),
						),
						500
					);
					exit();	



				}else{


					return $this->response(
						array(
	            			'status' => array(
	            				'type'=>'ok',
	            				'msg'=>'Participation accepted.',
	            				'ERRNO' => null,
	            			),
	            			'participation'=>$participation,
	            		),
	            		200
       				);

				}

				

			}
		}



	}

	public function get_user_reject_participation($race_id){

		if(!$this->apiapp){
			return $this->response;
			exit();
		}

		$this->verify_api_authtoken();

		if(!$this->apiauthtoken){
			return $this->response;
			exit();
		}


		if($race_id == null){
			//wrong/inexistent required parameters

			return $this->response(
				array(
    				'status' => array(
    					'type'=>'error',
    					'msg'=>'Inexistent required parameters: (race_id).',
    					'ERRNO'=> 'EINVAL',
    				),
    			),
    			400
    		);
    		exit();
		}

		$race = Model_Race::find($race_id);

		if(!$race){

			return $this->response(
				array(
    				'status' => array(
    					'type'=>'error',
    					'msg'=>'Race not found ('.$race_id.').',
    					'ERRNO' => 'ENOENT',
    				),
    			),
    			404
    		);
    		exit();
		}

		$participation = DB::select()->from('participations')->as_object('Model_Participation')->where_open()
    		->where('user_id', $this->apiauthtoken->user->id)
    		->and_where('race_id', $race->id)
		->where_close()->execute()->as_array();

		if(!$participation){

			return $this->response(
				array(
    				'status' => array(
    					'type'=>'ok',
    					'msg'=>'Participation to this race does not exist. Nothing to reject.',
    					'ERRNO' => null,
    				),
    			),
    			200
    		);
    		exit();

		}else{

			if(!$participation[0]->delete()){

				return $this->response(
						array(
			    			'status' => array(
			    				'type'=>'error',
			    				'msg'=>'Couldn\'t remove participation',
			    				'ERRNO' => 'EWRENT',
			    			),
			    		),
			    		500
			    	);

	    		exit();				

			}else{

				return $this->response(
						array(
			    			'status' => array(
			    				'type'=>'ok',
			    				'msg'=>'Participation successfully rejected.',
			    				'ERRNO' => null,
			   				),
			   			),
			   			200
			   		);

			    exit();

			}

		}


	}

	public function get_user_participation_at_race($race_id=null){

		if(!$this->apiapp){
			return $this->response;
			exit();
		}

		$this->verify_api_authtoken();

		if(!$this->apiauthtoken){
			return $this->response;
			exit();
		}


		if($race_id == null){
			//wrong/inexistent required parameters

			return $this->response(
				array(
    				'status' => array(
    					'type'=>'error',
    					'msg'=>'Inexistent required parameters: (race_id).',
    					'ERRNO'=> 'EINVAL',
    				),
    			),
    			400
    		);
    		exit();
		}

		$race = Model_Race::find($race_id);

		if(!$race){

			return $this->response(
				array(
    				'status' => array(
    					'type'=>'error',
    					'msg'=>'Race not found ('.$race_id.').',
    					'ERRNO' => 'ENOENT',
    				),
    			),
    			404
    		);
    		exit();
		}

		$participation = DB::select()->from('participations')->as_object('Model_Participation')->where_open()
    		->where('user_id', $this->apiauthtoken->user->id)
    		->and_where('race_id', $race->id)
		->where_close()->execute()->as_array();




		if(!$participation){

			return $this->response(
				array(
    				'status' => array(
    					'type'=>'error',
    					'msg'=>'Particiption not found for this race.',
    					'ERRNO'=> 'ENOENT',
    				),
    			),
    			400
    		);

		}else{
			$participation[0]->stat;

			return $this->response(
				array(
    				'status' => array(
    					'type'=>'ok',
    					'msg'=>'Participation found.',
    					'ERRNO'=> null,
    				),
    				'participation' => $participation[0],
    			),
    			200
    		);

		}

	}


	public function get_gpx_data_for_race($race_id=null){

		if(!$this->apiapp){
			return $this->response;
			exit();
		}


		if($race_id == null){
			//wrong/inexistent required parameters

			return $this->response(
				array(
    				'status' => array(
    					'type'=>'error',
    					'msg'=>'Inexistent required parameters: (race_id).',
    					'ERRNO'=> 'EINVAL',
    				),
    			),
    			400
    		);
    		exit();
		}

		$race = Model_Race::find($race_id);

		if(!$race){

			return $this->response(
				array(
    				'status' => array(
    					'type'=>'error',
    					'msg'=>'Race not found ('.$race_id.').',
    					'ERRNO' => 'ENOENT',
    				),
    			),
    			404
    		);
    		exit();
		}


		return $this->response(
				array(
    				'status' => array(
    					'type'=>'ok',
    					'msg'=>'GPX data.',
    					'ERRNO' => null,
    				),
    				'gpx' => $race->gpx,
    			),
    			200
    		);


	}

	public function get_races()
	{
		if(!$this->apiapp){
			return $this->response;
			exit();
		}

		$data = Model_Race::find('all', array(
    		'where' => array(
        		array('public', 1),
        		array('state', 'enabled'),
    		),
    		'related' => array('participations','user')
		));
		

		$this->response($data);
	}


/////////////////////////////////////////////////////NOT FINISHED//////////////////////////////////////////////////////////////
	//these function are not finished 


	public function get_race($id)
	{
		if(!$this->apiapp){
			return $this->response;
			exit();
		}

		$data = Model_Race::find($id, array(
    		'where' => array(
        		array('public', 1),
        		array('state', 'enabled'),
    		),
    		'related' => array('participations', 'participations.user', 'user'),
		));

		$this->response($data);
	}


	public function get_race_users($id)
	{	
		if(!$this->apiapp){
			return $this->response;
			exit();
		}

		$data = Model_Race::find($id, array(
    		'where' => array(
        		array('public', 1),
        		array('state', 'enabled'),
    		),
    		'related' => array('participations', 'participations.user', 'user'),
		));

		$this->response($data);
	}

	public function get_me(){

		if(!$this->apiapp){
			return $this->response;
			exit();
		}

		$this->verify_api_authtoken();


		if(!$this->apiauthtoken){
			return $this->response;
			exit();
		}

		//This will fail miserably
		$id = Input::get("token");
		$user = Model_User::find($id, array('related' => "participations"));
		$this->response($user);
	}

}
