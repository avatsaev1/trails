<?php

class Controller_Me extends Controller_Base
{

	public function before (){

		parent::before();

		if(!isset($this->current_user)){
			Session::set_flash('error', 'To access your profile you need to login first.');
			Response::redirect('home');
		}
	}

	public function action_index()
	{
		$data["subnav"] = array('index'=> 'active' );
		$this->template->title = 'Me &raquo; Profile';
		$this->template->content = View::forge('me/index', $data);
	}

	public function action_races_view()
	{
		$input = Input::get('filter');
		
		$data["subnav1"] = array('races_view'=> 'active' );
		switch ($input)
		{ 
			case 'participating': 
			$result = DB::select()->from('participations')->as_object('Model_Participation')->where_open()
				->where('state', 'participating')
				->and_where('user_id', $this->current_user->id)
			->where_close()->execute()->as_array();	
			$data["participations"] =  $result;
			
			$data["subnav"] = array('Participating'=> 'active' );
			$this->template->title = 'Me &raquo; Races';
			$participations = Model_User::find($this->current_user->id)->participations;
			$this->template->content = View::forge('me/races_view', $data);
			break;
    
			case 'finished': 

			$result = DB::select()->from('participations')->as_object('Model_Participation')->where_open()
    			->where('state', 'finished')
    			->and_where('user_id', $this->current_user->id)
			->where_close()->execute()->as_array();	
			$data["participations"] =  $result;
			
			$data["subnav"] = array('Finished'=> 'active' );
			$this->template->title = 'Me &raquo; Races';
			$this->template->content = View::forge('me/races_view', $data);

			break;
    
			case 'pending': 
				$result = DB::select()->from('participations')->as_object('Model_Participation')->where_open()
					->where('state', 'pending')
					->and_where('user_id', $this->current_user->id)
				->where_close()->execute()->as_array();	
				
				$data["participations"] =  $result;
			
				$data["subnav"] = array('Pending'=> 'active' );
				$this->template->title = 'Me &raquo; Races';
				$this->template->content = View::forge('me/races_view', $data);
			break;
			
			case 'personal': 
				$result = DB::select()->from('races')->as_object('Model_Race')->where_open()
					->where('type', 'personal')
					->and_where('state', '!=', 'finished')
					->and_where('user_id', $this->current_user->id)
					->where_close()->execute()->as_array();		
				$data["races"]=$result;
				
				$data["subnav"] = array('Personal'=> 'active' );
				$this->template->title = 'Me &raquo; Races';
				$this->template->content = View::forge('me/races_view', $data);
			break;

		   default:
		   	$result = DB::select()->from('participations')->as_object('Model_Participation')->where_open()
				->where('state', 'participating')
				->and_where('user_id', $this->current_user->id)
			->where_close()->execute()->as_array();	
			$data["participations"] =  $result;
			
			$data["subnav"] = array('Participating'=> 'active' );
			$this->template->title = 'Me &raquo; Races';
			$participations = Model_User::find($this->current_user->id)->participations;
			$this->template->content = View::forge('me/races_view', $data);


		}
		
	}

	public function action_race_view($id = null, $id_p = null)
	{
		is_null($id) and Response::redirect('me');

		if ( ! $data['race'] = Model_Race::find($id))
		{
			Session::set_flash('error', 'Could not find race #'.$id);
			Response::redirect('me');
		}
		
		$result = DB::select()->from('participations')->as_object('Model_Participation')->where_open()
    		->where('user_id', $this->current_user->id)
    		->and_where('race_id', $id)
    		->and_where('state', 'participating')
		->where_close()->execute()->as_array();	

		if($result)
		{
			$data['participation']=true;
		}
		else
		{
			$data['participation']=false;
		}
		
		$stat = DB::select()->from('participations')->as_object('Model_Participation')->where_open()
    		->where('user_id', $this->current_user->id)
    		->and_where('race_id', $id)
    		->and_where('state', 'finished')
		->where_close()->execute()->as_array();
		
		if($stat)
		{
			$stats = DB::select()->from('stats')->as_object('Model_Stat')->where_open()
    		->where('race_id', $id)
    		->and_where('participation_id', $id_p)
			->where_close()->execute()->as_array();
			$data['stats']=$stats;
		}
	
		
		$data["subnav1"] = array('races_view'=> 'active' );

		$data["subnav"] = array('race_view'=> 'active' );
		$this->template->title = 'Me &raquo; Race view';
		$this->template->content = View::forge('me/race_view', $data);
	}

	public function action_race_new()
	{
		if (Input::method() == 'POST')
		{
			$val = Model_Race::validate('create');
			
			if ($val->run())
			{


				//if current user is runner he can't make a public race
				//runner can only make a personal private race
				$race_type = 'personal';
				$race_public = 0;
				

				//a coach can make public/private and personal race
				if($this->current_user->group_id == '8'){
					
					$race_type = Input::post('type');


					if($race_type=='personal'){// if a race is personal it can't be public (visible by others)
						$race_public = 0;
					
					}else{//Type 'race' can be either public or invitation only
						$race_public = Input::post('public');
					}
					
				}

				$race = Model_Race::forge(array(
					'user_id' => $this->current_user->id,
					'title' => Input::post('title'),
					'description' => Input::post('description'),
					'datetime' => Input::post('datetime'),
					'zipcode' => Input::post('zipcode'),
					'town' => Input::post('town'),
					'public' => $race_public ,
					'type' => $race_type,
					'track_url' => Input::post('track_url'),
					'state' => 'enabled',
					'gpx' => rawurlencode(Input::post('gpx')),
					'coords' => '',
				));

				if ($race and $race->save())
				{
					if($race->type == 'personal')
					{
						list($insert_id, $rows_affected) = DB::insert('participations')->set(array(
						'user_id' => $this->current_user->id,
						'race_id' => $race->id,
						'state' => 'personal',
						))->execute();
					}
					
					Session::set_flash('success', 'Added race '.$race->title);

					Response::redirect('me/races_view');
				}

				else
				{
					Session::set_flash('error', 'Could not save race.');
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
			}

			Response::redirect('me/races_view');
		}
        $this->template->title = "New race";
		$this->template->content = View::forge('me/race_new');

	}
	
	public function action_race_edit($id = null)
	{
		is_null($id) and Response::redirect('me');
		
		$result = DB::select()->from('races')->as_object('Model_Race')->where_open()
			->where('user_id', $this->current_user->id)
			->and_where('id', $id)
			->where_close()->execute()->as_array();
		
		if(!$result)
		{
			Session::set_flash('error', 'Could not edit race : You are not the owner.');
			Response::redirect('races');
		}	
		
		else
		{
			if ( ! $race = Model_Race::find($id))
			{
				Session::set_flash('error', 'Could not find race #'.$id);
				Response::redirect('races');
			}

			$val = Model_Race::validate('edit');

			if ($val->run())
			{
				//if current user is runner he can't make a public race
				//runner can only make a personal private race
				$race_type = 'personal';
				$race_public = 0;
				

				//a coach can make public/private and personal race
				if($this->current_user->group_id == '8'){
					
					$race_type = Input::post('type');


					if($race_type=='personal'){// if a race is personal it can't be public (visible by others)
						$race_public = 0;
					
					}else{//Type 'race' can be either public or invitation only
						$race_public = Input::post('public');
					}

			
			$race->user_id = $this->current_user->id;
			$race->title = Input::post('title');
			$race->description = Input::post('description');
			$race->datetime = Input::post('datetime');
			$race->zipcode = Input::post('zipcode');
			$race->town = Input::post('town');
			$race->public = $race_public;
			$race->type = $race_type;
			$race->track_url = Input::post('track_url');
			$race->state ='enabled';
			$race->gpx = rawurlencode(Input::post('gpx'));
			}


			if ($race->save())
			{
				Session::set_flash('success', 'Updated race' . $race->title);

				Response::redirect('me/races_manage');
			}

			else
			{
				Session::set_flash('error', 'Could not update race ' . $race->title);
			}
		}
		
		else
		{

			$this->template->set_global('race', $race, false);
		}



		$this->template->title = "Race Edit";
		$this->template->content = View::forge('me/race_edit');

		}

	}

	
	public function action_races_manage()
	{
		$result = DB::select()->from('races')->as_object('Model_Race')->where_open()
			->where('user_id', $this->current_user->id)
			->and_where('state', '!=', 'finished')
			->where_close()->execute()->as_array();		
			$data["races"]=$result;
				
			$data["subnav1"] = array('races_manage'=> 'active' );
			$this->template->title = 'Me &raquo; Races';
			$this->template->content = View::forge('me/races_manage', $data);
	}

}
