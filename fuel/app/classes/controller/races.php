<?php

class Controller_Races extends Controller_Base
{

	public function before (){

		parent::before();

		if(!isset($this->current_user)){
			Session::set_flash('error', 'To access races you need to login first.');
			Response::redirect('home');
		}
	}
	

	public function action_index()
	{
			$result = DB::select()->from('races')->as_object('Model_Race')->where_open()
    		->where('public', '1')
    		->and_where('type', 'race')
    		->and_where('state', '!=', 'finished')
    		->order_by('datetime', 'desc')
			->where_close()->execute()->as_array();		
		$data["races"]=$result;

	
		$this->template->title = "Races";

		$view = View::forge('races/index', $data);

		$this->template->content = $view;
		
	}

	public function action_view($id = null){
		
		is_null($id) and Response::redirect('me');
		
		$result = DB::select()->from('races')->as_object('Model_Race')->where_open()
    		->where('id', $id)
    		->where('public', 1)
    		->where('type', 'race')
    		->and_where('type', 'race')
			->where_close()->execute()->as_array();		
		$data["races"]=$result;

		if ( ! $result)
		{
			Session::set_flash('error', 'Could not find race.');
			Response::redirect('me');
		}
		
		$result = DB::select()->from('participations')->as_object('Model_Participation')->where_open()
    		->where('user_id', $this->current_user->id)
    		->and_where('race_id', $id)
    		->and_where('state', 'participating')
		->where_close()->execute()->as_array();	

		if($result)
		{
			$data['participation']=true;
		}
		else
		{
			$data['participation']=false;
		}

		$data["subnav"] = array('iew'=> 'active' );
		$this->template->title = 'Me &raquo; view';
		$this->template->content = View::forge('races/view', $data);	}


	public function action_participated($id = null)
	{
		is_null($id) and Response::redirect('races');
		
		$race = DB::select()->from('races')->as_object('Model_Race')->where_open()
					->where('type', '!=', 'personal')
					->and_where('state', '!=', 'finished')
					->and_where('id', $id)
					->where_close()->execute()->as_array();	
		if(!$race)	
		{
			
			
			Session::set_flash('error', 'Could not find race.');
			Response::redirect('races');

		}
		
		else
		{
		
			$result = DB::select()->from('participations')->as_object('Model_Participation')->where_open()
    			->where('user_id', $this->current_user->id)
				->and_where('race_id', $id)
				->where_close()->execute()->as_array();	
		
				if($result)
				{
		
					$result2 = DB::update('participations')
				->value('state', 'participating')
				->where('user_id', $this->current_user->id)
				->and_where('race_id', $id)
				->execute();
				
				Session::set_flash('success', 'Participate at race.');
				Response::redirect_back();
				}
		
				else
				{
					list($insert_id, $rows_affected) = DB::insert('participations')->set(array(
					'user_id' => $this->current_user->id,
					'race_id' => $id,
					'state' => 'participating',
					))->execute();
		
					Session::set_flash('success', 'Participate at race.');
					Response::redirect_back();
				}
		}
		
	}

	public function action_rejected($id = null)
	{
		$participation = DB::select()->from('participations')->as_object('Model_Participation')->where_open()
					->where('user_id', $this->current_user->id)
					->and_where('race_id', $id)
					->where_close()->execute()->as_array();	
		if(!$participation)	
		{
			
			
			Session::set_flash('error', 'Could not find participation.');
			Response::redirect('races');

		}
		
		else
		{

	
		$query = DB::delete('participations')
		->where('user_id', $this->current_user->id)
		->and_where('race_id', $id)
		->execute();
		
		Session::set_flash('success', 'Rejected participation at the race.');
		Response::redirect_back();
		}
	}
	
	public function action_finished($id = null)
	{
		 $result = DB::update('races')
			->value('state', 'finished')
			->where('user_id', $this->current_user->id)
			->and_where('id', $id)
			->execute();
		
		if(!$result)
		{
			Session::set_flash('error', 'Could not finished the race : You are not the owner.');
			Response::redirect('races');
	
		}
		else
		{
			 DB::update('participations')
			->value('state', 'finished')
			->where('race_id', $id)
			->execute();

		Session::set_flash('success', 'Closed the race.');
		Response::redirect_back();
		}
		
	}
	
	public function action_closed($id = null)
	{
		
		$result = DB::delete('races')
		->where('user_id', $this->current_user->id)
		->and_where('id', $id)
		->execute();
		
		if(!$result)
		{
			Session::set_flash('error', 'Could not closed the race: You are not the owner.');
			Response::redirect('races');
	
		}
		else
		{
		 	DB::delete('participations')
			->where('race_id', $id)
			->execute();

			Session::set_flash('success', 'Closed the race.');
			Response::redirect_back();
		}
	}

	public function action_partcipating_at_race($race_id)
	{
		$result = DB::select()->from('participations')->as_object('Model_Participation')->where_open()
    		->where('race_id', $race_id)
			->where_close()->execute()->as_array();	
		if($result)
		{
			return true;
		}
		
		else
		{
			return false;
		}
	}


}
