<?php
return array(
	'_root_'  => 'home/index',  // The default route
	'_404_'   => 'welcome/404',    // The main 404 route
	
	'hello(/:name)?' => array('welcome/hello', 'name' => 'hello'),
	'me/races/:id' => 'me/races_view',
	'me/races/new' => 'me/races_new',
	'api1/race/:id/accept_participation' => 'api1/user_accept_participation/$1',
	'api1/race/:id/reject_participation' => 'api1/user_reject_participation/$1',
	'api1/race/:id/user_participation' => 'api1/user_participation_at_race/$1',
	'api1/race/:id/gpx' => 'api1/gpx_data_for_race/$1',
);